var gulp = require('gulp'),
sass = require('gulp-sass'),
concat = require('gulp-concat'),
minifycss = require('gulp-minify-css'),
rename = require('gulp-rename'),
uglify = require('gulp-uglify'),
livereload = require('gulp-livereload');

gulp.task('styles', function() {
	return gulp.src('stylesheets/*.scss')
	.pipe(sass({ style: 'expanded' }))
	.pipe(gulp.dest('css'))
	.pipe(rename({suffix: '.min'}))
	.pipe(minifycss())
	.pipe(gulp.dest('dist/css'))
	.pipe(livereload());
});

gulp.task('scripts', function () {
	return gulp.src('javascripts/script.js')
	.pipe(uglify({outSourceMap: false}))
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dist/js'))
	.pipe(livereload());
});

gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('stylesheets/*.scss', ['styles']);
  gulp.watch('javascripts/script.js', ['scripts']);
  gulp.watch('dist/**').on('change', function(file) {
      livereload.changed(file.path);
  });
});

gulp.task('default', ['styles', 'scripts', 'watch']);