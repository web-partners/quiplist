'use strict';

(function($) {
	// Realtor Profile Photo Crop
	var croppedImgDataUrl = "";
	function handleFiles(t)
	{
		var fileList = t.files;
		for (var i = 0; i < fileList.length; i++)
		{
			var file = fileList[i];
			var blobSrc = window.URL.createObjectURL(file);
			$('#cropper-wrap img').cropper('replace', blobSrc);
			croppedImgDataUrl = $('#cropper-wrap > img').cropper('getDataURL', {
				width: 154,
  				height: 154
			});
			$('#realtor-avatar').val(croppedImgDataUrl);
			$('#preview-image-btn').removeAttr('disabled');
		}
	}
	$('#cropper-wrap img').cropper({
		aspectRatio: 1 / 1,
		crop: function(data){
			if(croppedImgDataUrl !== "") {
				croppedImgDataUrl = $('#cropper-wrap > img').cropper('getDataURL', {
				width: 154,
  				height: 154
			});
				$('#realtor-avatar').val(croppedImgDataUrl);
				$('#profile-preview').attr('src', croppedImgDataUrl);
			}
		}
	});
	$('#rotate-image-left-btn').on('click', function(){
		$('#cropper-wrap > img').cropper('rotate', -90);
	});
	$('#rotate-image-right-btn').on('click', function(){
		$('#cropper-wrap > img').cropper('rotate', 90);
	});
	$("#file1").on('change', function() {
	    $("#uploader1").val(this.files && this.files.length ? this.files[0].name : this.value.replace(/^C:\\fakepath\\/i, ''));
	    handleFiles(this);
	});

	// Unsaved Fields Check
	var unsaved = false;
	$(":input").on('change', function(){
	    unsaved = true;
	});
	function unloadPage(){ 
	    if(unsaved){
	        return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
	    }
	}
	window.onbeforeunload = unloadPage;
	// Codes
	$('span.code').on('click', function(){
		$(this).addClass('hidden').siblings('input[type="text"].edit-code').removeClass('hidden').focus();
	});
	$('input[type="text"].edit-code').on('keyup', function(){
		$(this).siblings('span.code').text($(this).val());
	}).on('blur', function(){
		$(this).addClass('hidden').siblings('span.code').removeClass('hidden').focus();
	});
})(jQuery);