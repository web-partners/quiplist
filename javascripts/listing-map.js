(function($){
    var latLng,
    address = $('#address').text();
    function showGoogleMaps() {
      var mapOptions = {
        zoom: 12,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: latLng
      };
      map = new google.maps.Map(document.getElementById('google-map'),
      mapOptions);
      marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP
      });
    }
    function getLatLng() {
      $.ajax({
        url: "http://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=false",
        type: "POST",
        success: function(res){
          latLng = new google.maps.LatLng(res.results[0].geometry.location.lat, res.results[0].geometry.location.lng);
          showGoogleMaps(); 
        }
      });
    }
    google.maps.event.addDomListener(window, 'load', getLatLng);
    google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });
})(jQuery);