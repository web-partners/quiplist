(function($){

	$('.home-modal a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});

	$('#search').find('section input[type="range"]').on('change input', function(){
		$(this).closest('section').find('input[type="number"]').val($(this).val());
	});
	$('#search').find('section input[type="number"]').on('change keyup', function(){
		var key = event.keyCode || event.charCode;
		if (key != 8 && key != 46)
		{
			$(this).closest('section').find('input[type="range"]').val($(this).val());
		}
	});
	$('.home-modal').find('input[type="text"]').on('focus', function() {
		if (this.value == this.defaultValue) {
			this.value = '';
		}
	}).on('blur', function() {
		if (this.value == '') { 
			this.value = this.defaultValue;
			$(this).css('text-align', 'center');
		}
		else {
			$(this).css('text-align', 'left');
		}
	});
	$('#homes-nearby-carousel').carousel({
		interval: false
	});
	$('.carousel[data-type="multi"] .item').each(function(){
	  var next = $(this).next();
	  if (!next.length) {
	    next = $(this).siblings(':first');
	  }
	  next.children(':first-child').clone().appendTo($(this));
	  
	  for (var i=0;i<2;i++) {
	    next=next.next();
	    if (!next.length) {
	    	next = $(this).siblings(':first');
	  	}
	    
	    next.children(':first-child').clone().appendTo($(this));
	  }
	});
	var homePhotosResize = function() {
		var slideHeight = $('#home-photos .item.active').height();
		$('#home-photos').find('.carousel-inner').css('height', slideHeight);
	}
	homePhotosResize();
	$(window).on('resize', function(){
		homePhotosResize();
	});
})(jQuery);