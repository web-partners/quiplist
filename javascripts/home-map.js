(function($){
    var position,
        currentState;

    function showGoogleMaps() {
   
        var latLng = new google.maps.LatLng(position[0], position[1]);
     
        var mapOptions = {
            zoom: 12,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: latLng
        };
     
        map = new google.maps.Map(document.getElementById('google-map'),
            mapOptions);

        marker = new google.maps.Marker({
            position: latLng,
            map: map,
            draggable: false,
            animation: google.maps.Animation.DROP
        });
    }

    $.get("http://ipinfo.io", function(response) {
        position = response.loc.split(',');
        currentState = response.region;
        google.maps.event.addDomListener(window, 'load', showGoogleMaps);
        $('#state-ddl').find('select').find('option:contains(' + currentState + ')').attr('selected', 'selected');
    }, "jsonp");
     
    google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });
  })(jQuery);